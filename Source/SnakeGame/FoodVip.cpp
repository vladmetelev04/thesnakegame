#include "FoodVip.h"
#include "SnakeBase.h"
#include "Interactable.h"

// Sets default values
AFoodVip::AFoodVip()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFoodVip::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFoodVip::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
