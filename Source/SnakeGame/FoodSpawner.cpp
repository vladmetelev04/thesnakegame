// FoodSpawner.cpp

#include "FoodSpawner.h"
#include "TimerManager.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
    // Set this actor to call Tick() every frame
    PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
    Super::BeginPlay();

    // Set a timer to spawn food every 8 seconds
    GetWorldTimerManager().SetTimer(SpawnFoodTimerHandle, this, &AFoodSpawner::SpawnFood, 8.0f, true);
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Spawn food at random location
void AFoodSpawner::SpawnFood()
{
    if (FoodClass)
    {
        // Get random location within specified bounds
        FVector SpawnLocation;

        // Keep generating random locations until a unique one is found
        do
        {
            SpawnLocation = FMath::RandPointInBox(SpawnAreaBounds);
        } while (SpawnedFoodLocations.Contains(SpawnLocation));

        // Add spawned location to list
        SpawnedFoodLocations.Add(SpawnLocation);

        // Spawn the food
        AFood* SpawnedFood = GetWorld()->SpawnActor<AFood>(FoodClass, SpawnLocation, FRotator::ZeroRotator);

        // If successfully spawned, notify
        if (SpawnedFood)
        {
            UE_LOG(LogTemp, Warning, TEXT("Spawned food at %s"), *SpawnLocation.ToString());
        }
        else
        {
            UE_LOG(LogTemp, Error, TEXT("Failed to spawn food!"));
        }
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("FoodClass is nullptr!"));
    }
}
