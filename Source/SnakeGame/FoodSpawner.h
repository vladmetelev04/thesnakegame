// FoodSpawner.h

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Food.h"
#include "FoodSpawner.generated.h"

UCLASS()
class AFoodSpawner : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AFoodSpawner();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    // Function to spawn food at random location
    void SpawnFood();

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Reference to the food class
    UPROPERTY(EditAnywhere, Category = "Spawning")
    TSubclassOf<class AFood> FoodClass;

    // Bounds of the spawning area
    UPROPERTY(EditAnywhere, Category = "Spawning")
    FBox SpawnAreaBounds;

private:
    // Timer handle for spawning food
    FTimerHandle SpawnFoodTimerHandle;

    // Array to store spawned food locations
    TArray<FVector> SpawnedFoodLocations;
};
