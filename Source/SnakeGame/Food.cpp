#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h" // ��������� ���� ������������ ����

// Sets default values
AFood::AFood()
{
    // Set this actor to call Tick() every frame
    PrimaryActorTick.bCanEverTick = true;

    // Create the mesh component
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
    RootComponent = MeshComponent; // Set the mesh component as the root component
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
    if (bIsHead)
    {
        auto Snake = Cast<ASnakeBase>(Interactor);
        if (Snake)
        {
            Snake->AddSnakeElement();
            Destroy();
            // Play destruction sound
            if (DestroySound)
            {
                UGameplayStatics::PlaySoundAtLocation(this, DestroySound, GetActorLocation());
            }
        }
    }
}

















